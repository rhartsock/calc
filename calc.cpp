# include <iostream>
# include <iomanip>
# include <string>
# include <cctype>
# include "proj2.h"

using namespace std;


        //Push
        void Stack::push(int num)
        {       
                head = new Node(num, head);
        }

        //Pop
        void Stack::pop(int &num)
        {
                Node *temp;
                
                if(isEmpty())
                {
                        cout << "Empty Stack!!" << endl;
                }

                else
                {
                        num = head->number;
                        temp = head;
                        head = head->next;
                        delete temp;
                }
        }

        //Is the stack empty?
        bool Stack::isEmpty()
        {
                if(!head)
                        return true;
                else
                        return false;
        }

        //Begin Main
        int main()
{
        Stack stack;
        int s1, s2;
        std::string input;

        do{
                cin >> input;

                if(input == "e")
                {}

                else if(isdigit(input[0]))
                {
                        int numerical_input = atoi(input.c_str());
                        stack.push(numerical_input);
                }

                else if (input == "+")
                {
                        stack.pop(s1);
                        stack.pop(s2);
                        int result = s2 + s1;
                        stack.push(result);
                }

                else if (input == "-")
                {
                        stack.pop(s1);
                        stack.pop(s2);
                        int result = s2 - s1;
                        stack.push(result);
                }

                else if (input == "*")
                {
                        stack.pop(s1);
                        stack.pop(s2);
                        int result = s2 * s1;
                        stack.push(result);
                }

                else if (input == "/")
                {
                        stack.pop(s1);
                        stack.pop(s2);
                        int result = s2 / s1;
                        stack.push(result);
                }

                else if (input == "u")
                {
                        stack.pop(s1);
                        s1 = -s1;
                        stack.push(s1);
                }

                else if (input == "p")
                {
                        bool test = stack.isEmpty();
                        stack.pop(s1);
                        if(test == false)
                        {
                        cout << s1 << endl;
                        }
                }
        }while(input != "e");
        
        return 0;
        cin.ignore();
}