#ifndef STACH_H
#define STACK_H

class Stack
{
private:
        class Node
        {
                friend class Stack;
                int number;
                Node *next;

                Node(int number1, Node *next1 = NULL)
                {
                        number = number1;
                        next = next1;
                }
        };
        Node *head;
public:
        Stack() {head = NULL;}
        void push(int);
        void pop(int &);
        bool isEmpty();
};
#endif